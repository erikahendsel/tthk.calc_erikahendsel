﻿using System.Collections.Generic;
using Calc.Core.Domain;

namespace Calc.ApplicationServices
{
    public class OperatorRegistry
    {
        private readonly Dictionary<string, IOperator> _operators = new Dictionary<string, IOperator>();

        public void RegOperator<T>(IOperator @operator)
        {
            _operators[@operator.Symbol] = @operator;
        }

        public bool IsKnowOperator(string s)
        {
            return _operators.ContainsKey(s);
        }

        public bool IsKnowOperator(string s, out IOperator op)
        {
            var exists = _operators.ContainsKey(s);
            op = exists
                ? GetOperator(s) 
                : null;

            return exists;
        }

        public IOperator GetOperator(string opString)
        {
            return _operators[opString];
        }
    }
}