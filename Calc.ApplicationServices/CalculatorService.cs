﻿using Calc.Core.ApplicationServices;
using Calc.Core.Domain;
using System;
using System.Collections.Generic;

namespace Calc.ApplicationServices
{
    public class Singleton
    {
        private static Singleton _instance;

        private Singleton()
        {
        }

        public static Singleton GetInstance()
        {
            return _instance ?? (_instance = new Singleton());
        }
    }

    public class CalculatorService : ICalculatorService
    {
        private readonly OperatorRegistry _opRegistry = new OperatorRegistry();

        public CalculatorService()
        {
            _opRegistry.RegOperator<BinaryCalcNode>(new AddOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new MultiplyOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new DivideOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new SubtractOperator());
        }

        public decimal EvalExpression(string expression)
        {
            var node = Parse(expression);
            return node.GetValue();
        }

        public ParsedNode[] Split(string text)
        {
            var list = new List<ParsedNode>();

            var currentValue = string.Empty;
            var innerExpression = string.Empty;
            var isInParenthesis = false;
            var parenthesisCount = -1;

            for (var i = 0; i < text.Length; ++i)
            {
                var c = text[i];
                if (c == ' ') 
                {
                    continue;
                }

                if (c == '(')
                {
                    parenthesisCount++;
                    if (!isInParenthesis)
                    {
                        isInParenthesis = true;
                        continue;
                    }
                }
                if (c == ')' && parenthesisCount-- == 0)
                {
                    var node = new ParsedNode(Parse(innerExpression));
                    innerExpression = string.Empty;
                    list.Add(node);

                    isInParenthesis = false;
                    continue;
                }

                if (isInParenthesis)
                {
                    innerExpression += c;
                    continue;
                }

                var opString = c.ToString();

                if (_opRegistry.IsKnowOperator(opString, out IOperator op))
                {
                    if (!string.IsNullOrEmpty(currentValue))
                    {
                        list.Add(new ParsedNode(currentValue, null));
                        currentValue = string.Empty;
                    }

                    list.Add(new ParsedNode(opString, op));
                }
                else
                {
                    currentValue += c;
                }
            }

            if (!string.IsNullOrEmpty(currentValue))
            {
                list.Add(new ParsedNode(currentValue, null));
                currentValue = string.Empty;
            }

            return list.ToArray();
        }

        public CalcNode Parse(string v)
        {
            var list = Split(v);
            return Parse(list);
        }

        public CalcNode Parse(ParsedNode[] list)
        {
            ParsedNode highestNode = GetHighestNode(list, out var i);
            list = ProcessNode(list, highestNode, i);

            if (list.Length > 1)
            {
                return Parse(list);
            }

            return list[0].CalcNode;
        }

        public ParsedNode[] ProcessNode(ParsedNode[] list, ParsedNode highestNode, int i)
        {
            var prev = list[i - 1];
            var next = list[i + 1];

            if (highestNode.Operator is IBinaryOperator binary)
            {
                var left = prev.IsCalcNode 
                    ? prev.CalcNode 
                    : new ValueCalcNode(prev.Value);

                var right = next.IsCalcNode 
                    ? next.CalcNode 
                    : new ValueCalcNode(next.Value);

                var calcNode = new BinaryCalcNode(binary, left, right);

                return list.Replace(i - 1, i + 1, new ParsedNode(calcNode));
            }

            return null;
        }

        public ParsedNode GetHighestNode(ParsedNode[] list, out int highestNodeIndex)
        {
            highestNodeIndex = -1;
            ParsedNode highestNode = null;
            for (var i = 0; i < list.Length; i++)
            {
                if (!list[i].IsOperator)
                {
                    continue;
                }

                var isHighest = highestNode == null
                    || highestNode.Operator.Priority < list[i].Operator.Priority;
                if (isHighest)
                {
                    highestNode = list[i];
                    highestNodeIndex = i;
                }
            }

            return highestNode;
        }
    }

    public class ParsedNode
    {
        public ParsedNode(CalcNode calcNode)
        {
            CalcNode = calcNode;
        }

        public ParsedNode(string value, IOperator @operator)
        {
            Value = value;
            Operator = @operator;
        }

        public string Value { get; set; }
        public IOperator Operator { get; set; }
        public CalcNode CalcNode { get; }
        public bool IsOperator => Operator != null;
        public bool IsCalcNode => CalcNode != null;
    }
}
