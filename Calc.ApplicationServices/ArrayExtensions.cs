﻿using System.Linq;

namespace Calc.ApplicationServices
{
    public static class ArrayExtensions
    {
        public static T[] Replace<T>(this T[] array, int fromIndex, int toIndex, T value)
        {
            var temp = array
                .Where((x, i) => i < fromIndex || i > toIndex)
                .ToList();

            temp.Insert(fromIndex, value);
            return temp.ToArray();
        }
    }
}