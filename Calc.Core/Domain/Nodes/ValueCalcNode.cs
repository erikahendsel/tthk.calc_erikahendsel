﻿using System.Globalization;

namespace Calc.Core.Domain
{
    public class ValueCalcNode : CalcNode
    {
        public ValueCalcNode(decimal value)
        {
            Value = value;
            StringValue = value.ToString(CultureInfo.InvariantCulture);
        }

        public ValueCalcNode(string value)
        {
            StringValue = value;
            if (decimal.TryParse(value, out var decimalValue))
            {
                Value = decimalValue;
            }
            else
            {
                // invalid format
                IsValid = false;
            }
        }

        public override decimal GetValue()
        {
            return Value;
        }

        public override string GetString()
        {
            return Value.ToString();
        }

        public decimal Value { get; }
        public string StringValue { get; }
        public bool IsValid { get; }
    }

}
