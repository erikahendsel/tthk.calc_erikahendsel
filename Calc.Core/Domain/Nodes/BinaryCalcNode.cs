﻿using System;
using Calc.Core.ServiceInterfaces.Operators;

namespace Calc.Core.Domain
{
    public class BinaryCalcNode : CalcNode
    {
        public BinaryCalcNode(IBinaryOperator @operator, CalcNode left, CalcNode right)
        {
            Operator = @operator;
            Left = left;
            Right = right;
        }

        public override decimal GetValue()
        {
            return Operator.Exec(Left.GetValue(), Right.GetValue());
        }

        public override string GetString()
        {
            return $"{Left.GetString()}{Operator.Symbol}{Right.GetString()}";
        }

        public IBinaryOperator Operator { get; }
        public CalcNode Left { get; }
        public CalcNode Right { get; }
    }
}
