﻿using Calc.Core.ServiceInterfaces.Operators;

namespace Calc.Core.Domain
{
    public class MultiplyOperator : IBinaryOperator
    {
        public string Symbol => "*";

        public int Priority => 2;

        public decimal Exec(decimal left, decimal right)
        {
            return left * right;
        }
    }
}
