using Calc.ApplicationServices;
using Calc.Core.Domain;
using System;
using Xunit;

namespace Calc.Test
{
    public class AddOperator
    {
        [Fact]
        public void Should_BeAbleToAddTwoNumbersTogether()
        {
            var op = new Core.Domain.AddOperator();
            var result = op.Exec(1, 2);
            Assert.Equal(3, result);
        }
    }

    public class CalculatorService
    {
        [Fact]
        public void Should_BeAbleToEvaluateTree()
        {
            var rootNode = new BinaryCalcNode(
                new Core.Domain.AddOperator(),
                new ValueCalcNode(1),
                new BinaryCalcNode(
                    new Core.Domain.MultiplyOperator(),
                    new ValueCalcNode(2),
                    new ValueCalcNode(3)
                )
            );

            var result = rootNode.GetValue();

            Assert.Equal(7, result);
        }

        [Fact]
        public void Should_BeAbleToReprasentTreeAsString()
        {
            var rootNode = new BinaryCalcNode(
                new Core.Domain.AddOperator(),
                new ValueCalcNode(1),
                new BinaryCalcNode(
                    new Core.Domain.MultiplyOperator(),
                    new ValueCalcNode(2),
                    new ValueCalcNode(3)
                )
            );

            var result = rootNode.GetString();

            Assert.Equal("1+2*3", result);
        }

        [Fact]
        public void Should_BeAbleToSplitExpressionToArray()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var array = calcService.Split("1+2*3");

            Assert.Equal(5, array.Length);

            var expextedArray = new[] {
                "1",
                "+",
                "2",
                "*",
                "3"
            };
            for (var i = 0; i < expextedArray.Length; i++)
            {
                Assert.Equal(expextedArray[i], array[i].Value);
            }
        }

        [Fact]
        public void Should_BeAbleToGetHighestNode()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var array = calcService.Split("1+2*3");

            var res = calcService.GetHighestNode(array, out var _);
            Assert.Equal("*", res.Operator.Symbol);
        }

        [Fact]
        public void Should_BeAbleToParseStringToTree()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var node = calcService.Parse("1+2*3");

            Assert.True(node is BinaryCalcNode);
            var binaryCalcNode = (BinaryCalcNode)node;

            Assert.True(binaryCalcNode.Operator is Core.Domain.AddOperator);
            var addOperator = (Core.Domain.AddOperator)binaryCalcNode.Operator;

            Assert.True(binaryCalcNode.Left is ValueCalcNode);
            var addLeft = (ValueCalcNode)binaryCalcNode.Left;

            Assert.Equal(1, addLeft.Value);

            Assert.True(binaryCalcNode.Right is BinaryCalcNode);
            var binaryCalcNode2 = (BinaryCalcNode)binaryCalcNode.Right;

            Assert.True(binaryCalcNode2.Operator is MultiplyOperator);

            Assert.True(binaryCalcNode2.Left is ValueCalcNode);
            Assert.Equal(2, ((ValueCalcNode)binaryCalcNode2.Left).Value);

            Assert.True(binaryCalcNode2.Right is ValueCalcNode);
            Assert.Equal(3, ((ValueCalcNode)binaryCalcNode2.Right).Value);
        }

        [Fact]
        public void Should_BeAbleToEvalExpression()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("1 + 2 * 3");
            Assert.Equal(7, res);
        }

        [Fact]
        public void Should_BeAbleToUnderstandParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("(1 + 2) * 3");
            Assert.Equal(9, res);
        }

        [Fact]
        public void Should_BeAbleToUnderstandMultipleParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("1 + (1 + 2) * (3 + 1)");
            Assert.Equal(13, res);
        }

        [Fact]
        public void Should_BeAbleToUnderstandNestedParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("2 * ((1 + 2) + (3 + 1))");
            Assert.Equal(14, res);
        }
    }
}
